package com.example.activityrequest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	public static final int REQUEST_CODE = 0;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main1);
		
		Button button = (Button) findViewById(R.id.button1);
		
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent i = new Intent(MainActivity.this, SecondActivity.class);

				i.putExtra("Value1", "This value one for ActivityTwo ");
				i.putExtra("Value2", "This value two ActivityTwo");
				
				// set the request code to any code you like,
				// you can identify the callback via this code
				startActivityForResult(i, MainActivity.REQUEST_CODE);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	  if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
	    if (data.hasExtra("returnKey1")) {
	      Toast.makeText(this, data.getExtras().getString("returnKey1"),
	        Toast.LENGTH_SHORT).show();
	    }
	  }
	} 
}
