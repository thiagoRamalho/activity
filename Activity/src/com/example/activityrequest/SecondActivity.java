package com.example.activityrequest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SecondActivity extends Activity{
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main2);
		
		Button voltar = (Button) findViewById(R.id.button1);
		
		Button button = (Button) findViewById(R.id.button2);
		
		voltar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// Prepare data intent 
				Intent data = new Intent();

				data.putExtra("returnKey1", "Retornando de SecondActivity");

				// Activity finished ok, return the data
				SecondActivity.this.setResult(RESULT_OK, data);

				SecondActivity.this.finish();
			}
		});
		
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent i = new Intent(SecondActivity.this, ThirdActivity.class);

				i.putExtra("Value1", "This value one for ActivityTwo ");
				i.putExtra("Value2", "This value two ActivityTwo");
				
				// set the request code to any code you like,
				// you can identify the callback via this code
				startActivityForResult(i, MainActivity.REQUEST_CODE);
			}
		});
		
	};
	
}
