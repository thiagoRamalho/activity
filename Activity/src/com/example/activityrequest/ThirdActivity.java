package com.example.activityrequest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ThirdActivity extends Activity{
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main3);
		
		Button button = (Button) findViewById(R.id.button1);
		
		button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// Prepare data intent 
				Intent data = new Intent();
				
				data.putExtra("returnKey1", "Swinging on a star. ");
				data.putExtra("returnKey2", "You could be better then you are. ");
				
				// Activity finished ok, return the data
				ThirdActivity.this.setResult(RESULT_OK, data);
				
				ThirdActivity.this.finish();
			}
		});
		
	};
	
}
